#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, render_template, jsonify, url_for, redirect, session, flash
from flask_login import LoginManager, UserMixin, login_user, login_required, current_user
from flask_oauthlib.client import OAuth
from werkzeug import security
import json
#import settings
from peewee import *
#from models import Marker, Users, AllowedGuilds, AllowedChar
import locale
import requests
import time
import xmltodict
import unicodedata

secret_key = 'laclesecrete'
client_id = 'client_id'
client_secret = 'client_secret'

# Configuration de la base de données SQLite
DATABASE = 'markers.db'
db = SqliteDatabase(DATABASE)

# Définition de la classe de modèle pour la table markers
class Marker(Model):
    id = IntegerField(primary_key=True)
    marker_id = CharField(unique=True)
    marker_type = CharField()
    lat = FloatField()
    lng = FloatField()
    mp_type = CharField(null=True)
    icon = CharField(null=True)
    name = CharField(null=True)
    quality = CharField(null=True)
    title = CharField(null=True)
    season = CharField(null=True)
    weather = CharField(null=True)
    tribe_affiliate = CharField(null=True)
    faction = CharField(null=True)

    class Meta:
        database = db
        table_name = 'markers'
        # constraints = [
        #     Check('marker_id LIKE "npc_%" OR marker_id LIKE "boss_%"'),
        #     Check('marker_type IN ("boss", "mp", "npc", "tribu")'),
        #     Check('quality IN ("suprème", "excellent", "choix", "fin", "base") OR quality IS NULL')
        # ]


class Users(UserMixin, Model):
    id = IntegerField(primary_key=True)
    name = CharField(unique=True)
    password = CharField(null=True)

    class Meta:
        database = db
        table_name = 'users'


class AllowedGuilds(Model):
    id = IntegerField(primary_key=True)
    guild_id = CharField(unique=True)
    guild_name = CharField(unique=True)

    class Meta:
        database = db
        table_name = 'AllowedGuilds'


class AllowedChar(Model):
    id = IntegerField(primary_key=True)
    char_name = CharField(unique=True)

    class Meta:
        database = db
        table_name = 'AllowedChar'
# Création de la table markers si elle n'existe pas
with db:
    db.create_tables([Users, Marker, AllowedGuilds, AllowedChar])

app = Flask(__name__)
app.secret_key = secret_key

login_manager = LoginManager()
login_manager.login_message = 'blah2'
login_manager.init_app(app)
login = LoginManager(app)
login.login_view = 'login'
# login.login_message = 'blah'

oauth = OAuth(app)

ryzom = oauth.remote_app(
    name='ryzom',
    consumer_key=client_id,
    consumer_secret=client_secret,
    request_token_params={
        'scope': 'public',
        'state': lambda: security.gen_salt(10)
    },
    base_url='https://me.ryzom.com/api/oauth/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://me.ryzom.com/api/oauth/token',
    authorize_url='https://me.ryzom.com/api/oauth/authorize',
)
# Configuration de la base de données SQLite
#DATABASE = 'markers.db'
#db = SqliteDatabase(DATABASE)

# API Ryzom
ryzom_api = 'https://api.ryzom.com/'
time_api = ryzom_api + 'time.php?format=xml'
weather_api = ryzom_api + 'weather.php?cycles=0'

class User(UserMixin, Model):
    id = IntegerField(primary_key=True)
    name = CharField(unique=True)
    password = CharField(null=True)


def normalize(text):
    """Supprime les accents, gère les apostrophes et met en minuscule."""
    if text is None:
        return ""
    # Remplacer les apostrophes par des versions normalisées (facultatif)
    text = text.replace("'", "'")  # Par exemple, remplacer ' par ’
    return "".join(
        c for c in unicodedata.normalize("NFKD", text) if not unicodedata.combining(c)
    ).lower()


@login.user_loader
def load_user(user_id):
    return Users.get(Users.id == int(user_id))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    name = request.form['name']
    try:
        user = Users.get(Users.name == name and Users.password == request.form['password'])
        login_user(user)
    except:
        # flash('merci de vérifier vos informations et réessayer', 'error')
        return redirect(url_for('home'))

    return redirect(url_for('home'))


@app.route('/authorize')
def authorize():
    return ryzom.authorize(callback=url_for('callback', _external=True))


@app.route('/callback', methods=['GET', 'POST'])
def callback():
    if not current_user.is_anonymous:
        return redirect(url_for('home'))

    response = ryzom.authorized_response()
    print(response)
    if response is None or response.get('access_token') is None:
        return 'Access denied: reason={} error={}'.format(
            request.args['error_reason'],
            request.args['error_description']
        )
    user_info = ryzom.get('user', token=response['access_token'])
    user_info_json = user_info.data
    session[user_info_json['id']] = (response['access_token'], '')
    print(user_info.data)
    if user_info_json['guild'] in [guild.guild_id for guild in AllowedGuilds]\
            or user_info_json['id'] in [char.char_name for char in AllowedChar]:
        try:
            user = Users.get(Users.name == user_info_json['id'])
        except:
            Users.create(name=user_info_json['id'])
        user = Users.get(Users.name == user_info_json['id'])
        # log the user in
        login_user(user)
        return redirect(url_for('home'))
    else:
        flash('merci de vérifier vos informations et réessayer', 'error')
        return redirect(url_for('login'))


@app.route('/', methods=['GET'])
@login_required
def home():
    return render_template('ryzomap.html')


@app.route('/markers', methods=['GET', 'POST'])
def markers():
    if request.method == 'POST':
        data = request.get_json()
        with open('markers.json', 'r') as f:
            markers = json.load(f)

        marker_id = data['id']
        marker_lat = data['lat']
        marker_lng = data['lng']
        marker_options = {'title': data['options']['title'] or '',
                          'iconUrl': data['options']['iconUrl'] or ''}

        marker_to_add = {'id': marker_id,
                         'lat': float(marker_lat),
                         'lng': float(marker_lng),
                         'options': marker_options
                         }
        if marker_to_add not in markers:
            markers.append(marker_to_add)
            with open('markers.json', 'w') as f:
                json.dump(markers, f)
        return jsonify({'success': True})
    
    if request.method == 'GET':
        with open('markers.json', 'r') as f:
            markers = json.load(f)
    return jsonify(markers)


@app.route('/del_markers/<marker_id>', methods=['DELETE'])
def del_markers(marker_id):
    with open('markers.json', 'r') as f:
        markers = json.load(f)

    for i in markers:
        if i['id'] == marker_id:
            markers.remove(i)
    with open('markers.json', 'w') as f:
        json.dump(markers, f)
    return jsonify({'success': True})


@app.route('/vortex_path', methods=['GET'])
def vortex_path():
    with open('vortex_path.json', 'r') as f:
        vortex_path = json.load(f)
    return jsonify(vortex_path)


@app.route('/region_area', methods=['GET'])
def region_area():
    with open('region_areas.json', 'r') as f:
        region_areas = json.load(f)
    return jsonify(region_areas)


# Route pour récupérer les enregistrements filtrés sur le champ marker_type de la table markers
@app.route('/<marker_type>')
def get_markers(marker_type):
    if marker_type == 'poi':
        markers = Marker.select().where(Marker.marker_type.in_(['guild_agent', 'vortex', 'TP', 'temple', 'boss_marau', 'NH']))
    else:
        markers = Marker.select().where(Marker.marker_type == marker_type)

    results = [{'marker_id': m.marker_id,
                'marker_type': m.marker_type,
                'lat': m.lat,
                'lng': m.lng,
                'mp_type': m.mp_type,
                'icon': m.icon,
                'name': m.name,
                'quality': m.quality,
                'title': m.title,
                'season': m.season,
                'weather': m.weather,
                'tribe_affiliate': m.tribe_affiliate,
                'faction': m.faction} for m in markers]
    return jsonify(results)


@app.route('/florist')
def florist():
    response = requests.get(time_api)
    jsondata = xmltodict.parse(response.text)
    s = (jsondata['shard_time']['season'])
    currentseason = ["printemps", "été", "automne", "hiver"][int(s)]

    florist_spots = Marker.select().where(Marker.marker_type == 'florist', Marker.season == currentseason)

    results = [{'marker_id': m.marker_id,
                'marker_type': m.marker_type,
                'lat': m.lat,
                'lng': m.lng,
                'mp_type': m.mp_type,
                'icon': m.icon,
                'name': m.name,
                'quality': m.quality,
                'title': m.title,
                'season': m.season,
                'weather': m.weather,
                'tribe_affiliate': m.tribe_affiliate,
                'faction': m.faction} for m in florist_spots]
    return jsonify(results)


@app.route('/next_season')
def next_season():
    now = int(time.time())
    response = requests.get(time_api)
    jsondata = xmltodict.parse(response.text)

    locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')

    h = int(jsondata['shard_time']['time_of_day'])
    s = (jsondata['shard_time']['season'])

    e = 388800 - ((int(jsondata['shard_time']['day_of_season']) * 4320) + (h * 180))
    dd = 0
    hh = 0
    mm = 0
    ss = (now + e) - now

    while ss >= 86400:
        dd += 1
        ss -= 86400

    while ss >= 3600:
        hh += 1
        ss -= 3600

    while ss >= 60:
        mm += 1
        ss -= 60

    nextseason = ["l'été", "l'automne", "l'hiver", 'le printemps'][int(s)]
    nexttime = time.strftime("%A %d %B à %Hh%M", time.localtime(now + e))

    next_season_string = '%s débute dans:<br />%d jours, %d heures et %d minutes<br />ce %s' % (nextseason, dd, hh, mm, nexttime)

    return jsonify({"message": next_season_string})


@app.route('/weather')
def weather():
    response = requests.get(weather_api)
    return jsonify(response.text)


# Route pour la recherche de marqueurs
@app.route('/search')
def search_markers():
    query = request.args.get('search')
    print(query)
    # recupere saison:printemps,type:lune,quality:excellent dans un dict
    query_dict = dict()
    if ',' in query:
        for item in query.split(','):
            key, value = item.split(':')
            query_dict[key] = value
        print(query_dict)
    else:
        print(query)

    if query:
        search_terms = query.split('.')

        if len(search_terms) == 1:
            # Recherche des marqueurs correspondant à la requête
            query_normalized = normalize(query)
            print(query_normalized)
            print(normalize("Compagnie de l'Arbre Eternel"))
            markers = Marker.select().where(
                (fn.LOWER(Marker.title).contains(query)) |
                (fn.LOWER(Marker.name).contains(query)) |
                (fn.LOWER(Marker.mp_type).contains(query)) |
                (fn.LOWER(Marker.weather).contains(query)) |
                (fn.LOWER(Marker.quality).contains(query)) |
                (fn.LOWER(Marker.season).contains(query)) |
                (fn.LOWER(Marker.tribe_affiliate).contains(query))
            )

            # Conversion des résultats en liste de dictionnaires pour le format JSON
            results = [{'marker_id': m.marker_id,
                        'marker_type': m.marker_type,
                        'lat': m.lat,
                        'lng': m.lng,
                        'mp_type': m.mp_type,
                        'icon': m.icon,
                        'name': m.name,
                        'quality': m.quality,
                        'title': m.title,
                        'season': m.season,
                        'weather': m.weather,
                        'tribe_affiliate': m.tribe_affiliate,
                        'faction': m.faction} for m in markers]
            print(results)
            print(markers.count())
            # Renvoi des résultats au format JSON
            return jsonify(results)
        elif len(search_terms) == 2:
            markers = Marker.select().where(
                ((Marker.season == search_terms[0]) | (Marker.title == search_terms[0])) &
                ((Marker.title == search_terms[1]) | (Marker.mp_type == search_terms[1]) | (Marker.weather.contains(search_terms[1])) | (Marker.quality == search_terms[1]))
            )
            # Conversion des résultats en liste de dictionnaires pour le format JSON
            results = [{'marker_id': m.marker_id,
                        'marker_type': m.marker_type,
                        'lat': m.lat,
                        'lng': m.lng,
                        'mp_type': m.mp_type,
                        'icon': m.icon,
                        'name': m.name,
                        'quality': m.quality,
                        'title': m.title,
                        'season': m.season,
                        'weather': m.weather} for m in markers]
            # Renvoi des résultats au format JSON
            return jsonify(results)
        elif len(search_terms) == 3:
            markers = Marker.select().where(
                (Marker.season == search_terms[0]) &
                ((Marker.title == search_terms[1]) | (Marker.mp_type == search_terms[1])) &
                (Marker.quality == search_terms[2])
            )
            # Conversion des résultats en liste de dictionnaires pour le format JSON
            results = [{'marker_id': m.marker_id,
                        'marker_type': m.marker_type,
                        'lat': m.lat,
                        'lng': m.lng,
                        'mp_type': m.mp_type,
                        'icon': m.icon,
                        'name': m.name,
                        'quality': m.quality,
                        'title': m.title,
                        'season': m.season,
                        'weather': m.weather} for m in markers]
            # Renvoi des résultats au format JSON
            return jsonify(results)
    else:
        # Renvoi d'une erreur si la requête ne contient pas de paramètre "query"
        return jsonify({'error': 'Missing query parameter'}), 400


if __name__ == '__main__':
    app.run(debug=True) 

$(function () {
    var map_center = [6336, -13504]
    var map = Ryzom.map('map', {rzMode: 'world',
                                fullscreenControl: true,
                                fullscreenControlOptions: {
                                    position: 'topleft',
                                    keyboard: true
                                },
                                preferCanvas: true,
                                renderer: L.canvas(),
                            });
    map.setView(map_center, 6);
    $('.leaflet-control-attribution').hide()

    L.easyButton('<i class="iconify" data-icon="fluent:weather-hail-day-20-filled"></i>', function(btn, map){
        window.open('https://ryzom.eu.org', '_blank');
    }, "Météo d'Atys").addTo(map);

    const couleurs = {
        "50": "#A9C9D6",
        "100": "#F2E2A0",
        "150": "#F4B686",
        "200": "#F28C8C",
        "250": "#B9A0D1"};
    const opacity = 0.25;

    var customControl;
    // Fonction pour mettre à jour le contrôle personnalisé
    function updateSeasonControl() {
        // Vérifier si la case à cocher est cochée
        var checkbox = document.getElementById('nextseason');
        if (!checkbox.checked) {
            // Si la case n'est pas cochée, retirer le contrôle personnalisé s'il existe
            if (customControl) {
                customControl.remove();
                customControl = null;  // Remettre à null après avoir retiré le contrôle
            }
            return;  // Ne pas effectuer la requête AJAX
        }

        // La case est cochée, effectuer la requête AJAX
        $.getJSON("/next_season", function(controlContent) {
            var customControlContent = '<div id="customControl">' + controlContent.message;
            customControlContent += '</div>';

            // Retirer le contrôle personnalisé s'il existe déjà
            if (customControl) {
                customControl.remove();
            }

            // Créer le contrôle personnalisé
            customControl = L.control({position: 'bottomright'});
            customControl.onAdd = function(map) {
                var div = L.DomUtil.create('div', 'leaflet-bar');
                div.innerHTML = customControlContent;
                return div;
            };

            customControl.addTo(map);
        });
    }

    // Appeler la fonction initiale
    updateSeasonControl();

    // Mettre à jour automatiquement toutes les 30 minutes
    var intervalId = setInterval(function() {
        updateSeasonControl();
    }, 30 * 60 * 1000); // 30 minutes en millisecondes

    const sidepanelRight = L.control.sidepanel('mySidepanelRight', {
        panelPosition: 'right',
        tabsPosition: 'top',
        pushControls: true,
        darkMode: true,
        startTab: 'tab-1'
    }).addTo(map);

    var iconMapping = {
        best: L.icon({ iconUrl: 'static/images/best.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
        good: L.icon({ iconUrl: 'static/images/good.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
        bad: L.icon({ iconUrl: 'static/images/bad.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
        worst: L.icon({ iconUrl: 'static/images/worst.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
    };

    var WeathercontinentPositions = {
        fyros: {lat: 17976, lng: -24820},
        matis: {lat: 3712, lng: -3024},
        tryker: {lat: 17948, lng: -30560},
        zorai: {lat: 9572, lng: -2760},
        nexus: {lat: 8876, lng: -6936},
        newbieland: {lat: 9200, lng: -10824},
        kitiniere: {lat: 2248, lng: -16616},
        matis_island: {lat: 14684, lng: -688},
        route_gouffre: {lat: 6288, lng: -12640},
        terre: {lat: 1676, lng: -14080},
        bagne: {lat: 936, lng: -10228},
        sources: {lat: 3196, lng: -10248},
    };

    // Créer un contrôle de champ de recherche animé
    var AnimatedSearchBox = L.Control.extend({
        onAdd: function(map) {
            var container = L.DomUtil.create('div', 'animated-search-box');

            var form = L.DomUtil.create('form', '', container);
            form.id = 'searchbar';

            var input = L.DomUtil.create('input', 'search-input', form);
            input.type = 'search';
            input.id = 'search';
            input.placeholder = 'Rechercher...';

            L.DomEvent.addListener(input, 'keydown', function(event) {
                // Arrêter la propagation de l'événement de la touche
                L.DomEvent.stopPropagation(event);
            });

            var searchIcon = L.DomUtil.create('div', 'search-icon', container);
            searchIcon.innerHTML = '<img src="static/images/search_icon.png">';

            L.DomEvent.disableClickPropagation(container);

            L.DomEvent.addListener(searchIcon, 'click', function() {
                input.classList.toggle('active');
            });

            return container;
        }
    });

    // Ajouter le contrôle de champ de recherche à la carte
    new AnimatedSearchBox({ position: 'topright' }).addTo(map);

    // récupérer le JSON contenant les coordonnées des polylines
    $.getJSON('/vortex_path', function(data) {
        // parcourir chaque objet dans le tableau de données
        $.each(data, function(index, obj) {
            var polyline = L.polyline(obj.coords, obj.style);
            if (obj.popup) {
            polyline.bindPopup(obj.popup);
            }

            if (obj.layergroup === 'path') {
                polyline.addTo(layerGroups.pathLayerGroup);
            }
            else {
                polyline.addTo(layerGroups.VortexLayerGroup);
            }
        });


    });

    // afficher chaque polyline si le niveau de zoom est inférieur ou égal à 6
    map.on('zoomend', function() {
        var zoom = map.getZoom();
        if (zoom <= 6) {
            layerGroups.VortexLayerGroup.eachLayer(function(layer) {
                if (layer instanceof L.Polyline) {
                    layer.setStyle({opacity: 1});
                }
            });
            document.getElementById("WeatherLayerGroup").checked = true;
            updateLayers()
        } else {
            layerGroups.VortexLayerGroup.eachLayer(function(layer) {
                if (layer instanceof L.Polyline) {
                    layer.setStyle({opacity: 0});
                    }
            });
            document.getElementById("WeatherLayerGroup").checked = false;
            updateLayers()
        }
    });

    // Tableau pour stocker les markers
    var markers = [];
    const layerGroups = {tpLayerGroup: L.layerGroup(),
                         VortexLayerGroup: L.layerGroup(),
                         WeatherLayerGroup: L.layerGroup(),
                         pathLayerGroup: L.layerGroup(),
                         levelLayerGroup: L.layerGroup(),
                         tribeLayerGroup: L.layerGroup(),
                         kamitribeLayerGroup: L.layerGroup(),
                         karatribeLayerGroup: L.layerGroup(),
                         marautribeLayerGroup: L.layerGroup(),
                         affiliateLayerGroup: L.layerGroup(),
                         banditLayerGroup: L.layerGroup(),
                         outpostLayerGroup: L.layerGroup(),
                         GuildLayerGroup: L.layerGroup(),
                         BossLayerGroup: L.layerGroup(),
                         MarauderBossLayerGroup: L.layerGroup(),
                         NhLayerGroup: L.layerGroup(),
                         NpcLayerGroup: L.layerGroup(),
                         TempleLayerGroup: L.layerGroup(),
                         FlowerLayerGroup: L.layerGroup(),
                         worstTrykerLayerGroup: L.layerGroup(),
                         badTrykerLayerGroup: L.layerGroup(),
                         goodTrykerLayerGroup: L.layerGroup(),
                         bestTrykerLayerGroup: L.layerGroup(),
                         worstMatisLayerGroup: L.layerGroup(),
                         badMatisLayerGroup: L.layerGroup(),
                         goodMatisLayerGroup: L.layerGroup(),
                         bestMatisLayerGroup: L.layerGroup(),
                         worstZoraiLayerGroup: L.layerGroup(),
                         badZoraiLayerGroup: L.layerGroup(),
                         goodZoraiLayerGroup: L.layerGroup(),
                         bestZoraiLayerGroup: L.layerGroup(),
                         worstFyrosLayerGroup: L.layerGroup(),
                         badFyrosLayerGroup: L.layerGroup(),
                         goodFyrosLayerGroup: L.layerGroup(),
                         bestFyrosLayerGroup: L.layerGroup()
                         };

    // Fonction pour activer ou désactiver les layersGroups
//    function updateLayers() {
//        const checkboxes = document.querySelectorAll(".layer-group");
//
//        checkboxes.forEach(checkbox => {
//            const layerGroup = layerGroups[checkbox.id];
//            if (checkbox.checked) {
//                map.addLayer(layerGroup);
//            } else {
//                map.removeLayer(layerGroup);
//            }
//        });
//    }



    const checkboxes = document.querySelectorAll(".layer-group");
    const filtersDiv = document.getElementById("filters");
    const tribeCheckbox = document.getElementById("tribeCheckbox");
    const factionRadios = document.querySelectorAll('input[name="factionFilter"]');
    const allRadio = document.getElementById("tribeLayerGroup");

    function updateLayers() {
        checkboxes.forEach(checkbox => {
            const layerGroup = layerGroups[checkbox.id];
            if (layerGroup) {
                if (checkbox.checked) {
                    map.addLayer(layerGroup);
                } else {
                    map.removeLayer(layerGroup);
                }
            }
        });

        if (tribeCheckbox.checked) {
            filtersDiv.style.display = "block";
            if (!Array.from(factionRadios).some(radio => radio.checked)) {
                allRadio.checked = true;
            }
            updateFactionLayers();
        } else {
            filtersDiv.style.display = "none";
            map.removeLayer(layerGroups["tribeLayerGroup"]);
            Object.keys(layerGroups).forEach(layer => {
                if (layer.includes("tribeLayerGroup")) {
                    map.removeLayer(layerGroups[layer]);
                }
            });
        }
    }


    function updateFactionLayers() {
        // Désactiver toutes les factions
        map.removeLayer(layerGroups["kamitribeLayerGroup"]);
        map.removeLayer(layerGroups["karatribeLayerGroup"]);
        map.removeLayer(layerGroups["marautribeLayerGroup"]);
        map.removeLayer(layerGroups["tribeLayerGroup"]);

        // Activer celle sélectionnée
        const selectedFaction = document.querySelector('input[name="factionFilter"]:checked');
        if (selectedFaction) {
            if (selectedFaction.id === "tribeLayerGroup") {
                map.addLayer(layerGroups["tribeLayerGroup"]);
            } else {
                map.addLayer(layerGroups[selectedFaction.id]);
            }
        }
    }

    // Ajouter des écouteurs d'événements
    checkboxes.forEach(checkbox => checkbox.addEventListener("change", updateLayers));
    factionRadios.forEach(radio => radio.addEventListener("change", updateFactionLayers));

    // Initialisation
    updateLayers();

    $.getJSON('/poi', function(data) {
        data.forEach(function(marker) {
            var icon = Ryzom.icon(marker.icon);
            var popupContent = marker.title;
            var layerGroup;
            
            if (marker.title === 'agent de guilde') {
                layerGroup = layerGroups.GuildLayerGroup;
            }
            else if (marker.title === 'vortex') {
                layerGroup = layerGroups.VortexLayerGroup;
            }
            else if (marker.title === 'temple kami' || marker.title === 'temple karavan') {
                layerGroup = layerGroups.TempleLayerGroup;
            }
            else if (marker.marker_type === 'boss_marau') {
                layerGroup = layerGroups.MarauderBossLayerGroup;
            }
            else if (marker.marker_type === 'NH') {
                layerGroup = layerGroups.NhLayerGroup;
            }
            else {
                layerGroup = layerGroups.tpLayerGroup;
            }
            
            var poi = L.marker([marker.lat, marker.lng], {icon: icon, title: marker.title}).addTo(layerGroup);
            poi.bindPopup(popupContent);
        });
    });

    // Récupérer les tribus
//    $.getJSON('/tribe', function(data) {
//    data.forEach(function(marker) {
//        var poi = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon), title: marker.title}).addTo(layerGroups.tribeLayerGroup);
//        poi.bindPopup('<b>' + marker.title + '</b>' +
//                      '<br>faction: ' + marker.faction);
//        });
//    });
    $.getJSON('/tribe', function(data) {
    // Définition des traductions

    data.forEach(function(marker) {
        var layerGroup;
        const faction = marker.faction;

        if (faction === "Kami") {
            layerGroup = layerGroups.kamitribeLayerGroup;
        } else if (faction === "Karavan") {
            layerGroup = layerGroups.karatribeLayerGroup;
        } else if (faction === "Maraudeurs") {
            layerGroup = layerGroups.marautribeLayerGroup;
        }

        var poi = L.marker([marker.lat, marker.lng], {
            icon: Ryzom.icon(marker.icon),
            title: marker.title
        }).addTo(layerGroup);
        poi.addTo(layerGroups.tribeLayerGroup);

        // Construire le contenu du popup
        var popupContent = '<b>' + marker.title + '</b>' +
                        '<br>' +
                        "faction: " + marker.faction;
        poi.bindPopup(popupContent);
        });
    });
    
    // Récupérer les bandits
    $.getJSON('/bandit', function(data) {
    data.forEach(function(marker) {
        var poi = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon), title: marker.title}).addTo(layerGroups.banditLayerGroup);
        poi.bindPopup(marker.title +
                        '<br>'
                        + marker.marker_id);
        });
    });

    // Récupérer les spots de fleuristes
    $.getJSON('/florist', function(data) {
    data.forEach(function(marker) {
        var poi = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon), title: marker.title}).addTo(layerGroups.FlowerLayerGroup);
        poi.bindPopup(marker.title +
                        '<br>'
                        + marker.marker_id);
        });
    });

    // Récupérer les ops
    $.getJSON('/outpost', function(data) {
    data.forEach(function(marker) {
        var outpost = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon), title: marker.title}).addTo(layerGroups.outpostLayerGroup);
        outpost.bindPopup(marker.title +
                        '<br>'
                        + marker.marker_id);
        });
    });

    // recupérer les NPC et les ajouter à la recherche
    $.getJSON('/NPC', function(data) {
        var npcNames = {};
        data.forEach(function(npc) {
            npcNames[npc.name] = true;
            var poi = L.marker([npc.lat, npc.lng], {icon: Ryzom.icon(npc.icon),
                                                        title: npc.name,
                                                        occupation: npc.title}
                            ).addTo(layerGroups.NpcLayerGroup);
            poi.bindPopup('<b>' + npc.name + '</b>' + '<br>'
                            + '<i>' + npc.title + '</i>' + '<br>'
                            + 'affilié: ' + npc.tribe_affiliate);
            // Vérifie si le champ tribe_affiliate est non nul
            if (npc.tribe_affiliate) {
                // Crée un marker pour le NPC dans affiliateLayerGroup
                var affiliatePoi = L.marker([npc.lat, npc.lng], {
                    icon: Ryzom.icon(npc.icon),
                    title: npc.name,
                    occupation: npc.title
                }).addTo(layerGroups.affiliateLayerGroup);

                affiliatePoi.bindPopup('<b>' + npc.name + '</b>' + '<br>'
                                       + '<i>' + npc.title + '</i>' + '<br>'
                                       + 'affilié: ' + npc.tribe_affiliate);
            }
        });

        var uniqueNames = Object.keys(npcNames);

        $("#search").autocomplete({
            appendTo:"#search.uiautocomplete-input",
            source: uniqueNames,
            autocomplete: true,
            minLength: 1,
        });
    });
    
    // Récupérer les rois
    $.getJSON('/boss', function(data) {
    data.forEach(function(marker) {
        var poi = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon),
                                                      title: marker.title}
                           ).addTo(layerGroups.BossLayerGroup);
        poi.bindPopup('Pop de roi<br>'
                        + marker.marker_id);
         });
    });

    // Récupérer les mp
    $.getJSON('/MP', function(data) {
    data.forEach(function(marker) {
        var mp = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon),
                                                     title: marker.title,
                                                     mp_type: marker.mp_type,
                                                     weather: marker.weather,
        });
        mp.bindPopup(marker.title + ' ' + marker.quality + ' ' + marker.season + ' ' + marker.weather + '<br>' + marker.marker_id);

        if (marker.season === "hiver") {
            if (marker.weather === "worst") {
                mp.addTo(layerGroups.worstTrykerLayerGroup);
            } else if (marker.weather === "bad") {
                mp.addTo(layerGroups.badTrykerLayerGroup);
            } else if (marker.weather === "good") {
                mp.addTo(layerGroups.goodTrykerLayerGroup);
            } else if (marker.weather === "best") {
                mp.addTo(layerGroups.bestTrykerLayerGroup);
            }
        } else if (marker.season === "printemps") {
            if (marker.weather === "worst") {
                mp.addTo(layerGroups.worstMatisLayerGroup);
            } else if (marker.weather === "bad") {
                mp.addTo(layerGroups.badMatisLayerGroup);
            } else if (marker.weather === "good") {
                mp.addTo(layerGroups.goodMatisLayerGroup);
            } else if (marker.weather === "best") {
                mp.addTo(layerGroups.bestMatisLayerGroup);
            }
        } else if (marker.season === "été") {
            if (marker.weather === "worst") {
                mp.addTo(layerGroups.worstZoraiLayerGroup);
            } else if (marker.weather === "bad") {
                mp.addTo(layerGroups.badZoraiLayerGroup);
            } else if (marker.weather === "good") {
                mp.addTo(layerGroups.goodZoraiLayerGroup);
            } else if (marker.weather === "best") {
                mp.addTo(layerGroups.bestZoraiLayerGroup);
            }
        } else if (marker.season === "automne") {
            if (marker.weather === "worst") {
                mp.addTo(layerGroups.worstFyrosLayerGroup);
            } else if (marker.weather === "bad") {
                mp.addTo(layerGroups.badFyrosLayerGroup);
            } else if (marker.weather === "good") {
                mp.addTo(layerGroups.goodFyrosLayerGroup);
            } else if (marker.weather === "best") {
                mp.addTo(layerGroups.bestFyrosLayerGroup);
            }
        }

        });
    });

    // Fonction pour ajouter un marker
    function addMarker(lat, lng, id, options) {
        if (!id) {
            id = 'marker-' + Math.random().toString(36).substr(2, 9);
            while (markers[id]) {
            id = generateUniqueId();
            }
        }
        var markerOptions = {
            title: 'pop roi',
            iconUrl: "static/images/kipee.png",
        };
        var marker = L.marker([lat, lng], {icon: Ryzom.icon('kipee'), title: markerOptions.title, id: id}).addTo(map);
        var popupContent = 'merci de confirmer la création du spot<br>ce ne sera pas sauvegardé sans votre intervention!<br>id:' + id + '<br><div style="display: flex; justify-content: space-between;"><button id="create-marker">Créer</button><button id="remove-' + id + '">Supprimer</button></div>';

        marker.bindPopup(popupContent);
        marker.on('popupopen', () => {
            document.getElementById('create-marker').addEventListener('click', () => {
                $.ajax({
                    url: '/markers',
                    type: 'POST',
                    data: JSON.stringify({ lat: lat, lng: lng, id: id, options: markerOptions }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function () {
                        alert('Le spot a été ajouté');
                    }
                });
                marker.closePopup();
            });
            document.getElementById('remove-' + id).addEventListener('click', () => {
                removeMarker(id);
            });
        });
        markers[id] = marker;
    }
    
    // Fonction pour supprimer un marker
    function removeMarker(id) {
        markers[id].remove();
        delete markers[id];
        $.ajax({
            url: '/del_markers/' + id,
            type: 'DELETE'
        });
    }

    // Récupérer les markers depuis le serveur via une requête AJAX
    $.getJSON('/markers', function(data) {
        data.forEach(function(marker) {
            addMarker(marker.lat, marker.lng, marker.id, marker.options);
        });
    });

    var markerLayer = L.featureGroup();
    function handleSearch(event) {
        event.preventDefault();
        const input = document.querySelector("#search");
        const value = input.value.toLowerCase();

        // Si la valeur de recherche est vide, supprimer tous les marqueurs de la carte
        if (value === "") {
            markerLayer.clearLayers();
            return;
        }

        map.removeLayer(markerLayer);
        // Créer un nouveau FeatureGroup pour les marqueurs filtrés
        const markers = new L.featureGroup();

        // Requête AJAX pour récupérer les marqueurs correspondant à la recherche
        $.getJSON('/search', {search: value}, function(data) {
        // Ajouter les marqueurs retournés au FeatureGroup
            data.forEach(function(markerData) {
                const marker = L.marker([markerData.lat, markerData.lng], {icon: Ryzom.icon(markerData.icon),
                                                                           title: markerData.title,
                                                                           mp_type: markerData.mp_type,
                                                                           weather: markerData.weather});

        let popupContent = "";
        if (markerData.marker_type === "MP") {
            popupContent += `<b>${markerData.mp_type} ${markerData.title}</b><br>`;
        } else if (markerData.marker_type === "tribe") {
            popupContent += `<b>${markerData.title}</b><br>`;
        } else if (markerData.name) {
            popupContent += `<b>${markerData.name}</b><br>`;
        }
        if (markerData.title && markerData.marker_type !== 'tribe' && markerData.marker_type !== 'MP') {
            popupContent += `<i>${markerData.title}</i><br>`;
        }
        if (markerData.tribe_affiliate) {
            popupContent += `Affilié : ${markerData.tribe_affiliate}<br>`;
        }
        if (markerData.quality) {
            popupContent += `Qualité : ${markerData.quality}<br>`;
        }
        if (markerData.season) {
            popupContent += `Saison : ${markerData.season}<br>`;
        }
        if (markerData.weather) {
            popupContent += `CC : <a href="https://ryzom.eu.org" target="_blank">${markerData.weather}</a><br>`;
        }
//        if (markerData.marker_id) {
//            popupContent += `Identifiant : ${markerData.marker_id}<br>`;
//        }

        marker.bindPopup(popupContent.trim());
                markers.addLayer(marker);
            });

        // Supprimer les anciens marqueurs de la carte et ajouter les nouveaux
        markerLayer = markers.addTo(map);

        // Centrer la carte sur le premier marqueur retourné
        if (data.length > 0) {
            const firstMarker = data[0];
            const latLng = L.latLng(firstMarker.lat, firstMarker.lng);
            map.setView(latLng); // Changer le niveau de zoom selon vos besoins
        }

      });
    }

    $.getJSON('/region_area', function (data) {
        data.forEach(polygon => {
          L.polygon(polygon.coordinates, {
            color: couleurs[polygon.level] || "#000000", // Noir si couleur inconnue
            fillOpacity: opacity
          }).addTo(layerGroups.levelLayerGroup);
        });
      }).fail(function () {
        console.error("Erreur lors du chargement du JSON.");
      });
      console.log(layerGroups.levelLayerGroup);

    // Écouter pour les clics sur la carte pour ajouter de nouveaux markers
    map.on('click', function(e) {
        var lat = e.latlng.lat;
        var lng = e.latlng.lng;
        var id = markers.length;
        var options = { title: 'pop roi', iconUrl: 'static/images/kipee.png' };
        addMarker(lat, lng, id, options);
    });

    // Ajout d'un écouteur d'événement pour les checkbox
    const CheckedlayerGroups = document.querySelectorAll(".layer-group");

    CheckedlayerGroups.forEach(layerGroup => {
        layerGroup.addEventListener("change", updateLayers);
        });;

    // Ajouter un écouteur pour la case à cocher
    var checkbox = document.getElementById('nextseason');
    checkbox.addEventListener('change', function() {
        // Mettre à jour le contrôle en fonction de l'état de la case à cocher
        updateSeasonControl();
    });

    // Ajout d'un écouteur d'événement pour le formulaire
    const form = document.getElementById("searchbar");
    form.addEventListener("submit", handleSearch);

    // Ajout d'un écouteur d'événement pour la touche "Entrée"
    const input = document.querySelector("#search");
    input.addEventListener("keydown", function(event) {
      if (event.key === "Enter") {
        handleSearch(event);
      }
    });

    // ecouteur d'evenements pour la navigation au clavier
    map.keyboard.disable();
    document.addEventListener('keydown', function(event) {
        var delta = 75;
        var key = event.code;
        console.log(event.code)

        if (key === 'F11') {
            if (document.fullscreenElement) {
                document.exitFullscreen();
            } else {
                document.documentElement.requestFullscreen();
            }
        } else if (key === 'NumpadAdd') {
            map.setZoom(map.getZoom() + 1);
        } else if (key === 'NumpadSubtract') {
            map.setZoom(map.getZoom() - 1);
        } else if (key === 'ArrowUp') {
            map.panBy([0, -delta]);
        } else if (key === 'ArrowDown') {
            map.panBy([0, delta]);
        } else if (key === 'ArrowLeft') {
            map.panBy([-delta, 0]);
        } else if (key === 'ArrowRight') {
            map.panBy([delta, 0]);
        }
    });

    function updateMap(data) {
        layerGroups.WeatherLayerGroup.clearLayers();
        weather_now = JSON.parse(data);

        Object.keys(weather_now.continents).forEach(function (continentName) {
            continent = continentName
            cycle = Object.keys(weather_now.continents[continentName])[0]
            condition = weather_now.continents[continentName][cycle].condition
            text = weather_now.continents[continentName][cycle].text

            var icon = iconMapping[condition];
            var position = WeathercontinentPositions[continent];

            var marker = L.marker([position.lat, position.lng], { icon: icon });
            marker.bindPopup(continentName + "<br>Météo: " + condition + "<br>Temps: " + text.substring(2));
            layerGroups.WeatherLayerGroup.addLayer(marker);
        });
    }

    function fetchData() {
        var weatherCheckbox = document.getElementById('WeatherLayerGroup');
        if (weatherCheckbox.checked) {
            $.getJSON('/weather', function(data) {
                updateMap(data);
                });
        };
        updateLayers();
    };

    // Effectuer un appel à l'API toutes les 1.5 minutes
    fetchData();
    setInterval(fetchData, 90000);

});

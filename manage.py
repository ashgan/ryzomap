#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import requests
import xml.etree.ElementTree as ET
import peewee
from fire import Fire
from peewee import SqliteDatabase
from models import Marker, AllowedGuilds, AllowedChar

# Configuration de la base de données SQLite
DATABASE = 'markers.db'
db = SqliteDatabase(DATABASE)
removed_item = list()


def create_marker():
    existing_markers = Marker.select().where(Marker.marker_type == 'boss').order_by(Marker.id.desc())
    last_id_number = int(existing_markers[0].marker_id.split('_')[1]) if existing_markers else 0
    print(existing_markers[0])
    with open('markers.json', 'r') as f:
        markers_data = json.load(f)

    for marker_data in markers_data:
        last_id_number += 1
        print(last_id_number)
        print(marker_data)
        new_marker = Marker(
            marker_id=f'boss_{last_id_number}',
            marker_type='boss',
            name='pop roi',
            title='pop roi',
            icon='tetekitin',
            lat=marker_data.get('lat', 0.0),
            lng=marker_data.get('lng', 0.0),
        )
        new_marker.save()
        print(f"Marker {marker_data.get('id')} inséré en base.")
        removed_item.append(marker_data)

    for i in removed_item:
        markers_data.remove(i)
    with open('markers.json', 'w') as f:
        json.dump(markers_data, f)


def delete_marker(marker_id):
    marker = Marker.get_or_none(marker_id=marker_id)
    if marker is not None:
        marker.delete_instance()
        print(f"Marker {marker_id} supprimé de la base.")
    else:
        print(f"Marker {marker_id} non trouvé.")


def view_guilds():
    for guild in AllowedGuilds.select():
        print(f'nom: {guild.guild_name}, guild_id: {guild.guild_id}')


def add_guild(guild_id, guild_name):
    if isinstance(guild_id, int):
        existing_id = AllowedGuilds.select().order_by(AllowedGuilds.id.desc())
        if len(list(existing_id)) == 0:
            current_id = 0
        else:
            current_id = list(existing_id)[0].id
        try:
            new_guild = AllowedGuilds.create(id=current_id + 1,
                                             guild_id=guild_id,
                                             guild_name=guild_name)
            print(f'guilde {guild_name} (id {guild_id}) ajoutée')
        except peewee.IntegrityError:
            print(f'guilde {guild_name} (id {guild_id}) déjà présente')
    else:
        print(f"l'ID de guilde {guild_id} est incorrect")


def del_guild(guild_id):
    guild_to_remove = AllowedGuilds.get_or_none(guild_id=guild_id)
    if guild_to_remove is not None:
        guild_to_remove.delete_instance()
        print(f"Guilde {guild_id} supprimée de la base.")
    else:
        print(f"Guilde {guild_id} non trouvée.")


def view_players():
    for player in AllowedChar.select():
        print(f'nom: {player.char_name}')


def add_player(player_name):
    existing_id = AllowedChar.select().order_by(AllowedChar.id.desc())
    if len(list(existing_id)) == 0:
        current_id = 0
    else:
        current_id = list(existing_id)[0].id
    try:
        new_player = AllowedChar.create(id=current_id + 1,
                                 char_name=player_name)
        print(f'personnage {player_name} ajouté')
    except peewee.IntegrityError:
        print(f'personnage {player_name} déjà présent')


def del_player(player_name):
    char_to_remove = AllowedChar.get_or_none(char_name=player_name)
    if char_to_remove is not None:
        char_to_remove.delete_instance()
        print(f"Personnage {player_name} supprimé de la base.")
    else:
        print(f"Personnage {player_name} non trouvé.")


def search_guild(searched_guild):
    reponse = requests.get('https://api.ryzom.com/guilds.php')
    if reponse.status_code == 200:
        contenu_xml = reponse.content

        # Parser le XML
        arbre = ET.fromstring(contenu_xml)
        guild_found = False
        # Trouver toutes les guilds et extraire leur nom
        for guild_element in arbre.findall('./guild'):
            nom_guild = guild_element.find('name').text
            guild_id = guild_element.find('gid').text
            if nom_guild is None:
                nom_guild = ''
            if searched_guild.lower() in nom_guild.lower() :
                print(f"Nom de la guilde : {nom_guild}, id: {guild_id}")
                guild_found = True
        if guild_found is False:
            print(f"Guilde {searched_guild} non trouvée")
    else:
        print(f"La requête a échoué avec le code HTTP {reponse.status_code}")


def main():
    Fire({
        'import': create_marker,
        'delete': delete_marker,
        'search_guild': search_guild,
        'view_guilds': view_guilds,
        'add_guild': add_guild,
        'del_guild': del_guild,
        'view_players': view_players,
        'add_player': add_player,
        'del_player': del_player,
    })
    db.close()

if __name__ == '__main__':
    main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from peewee import *
from flask_login import UserMixin

# Configuration de la base de données SQLite
DATABASE = 'markers.db'
db = SqliteDatabase(DATABASE)

# Définition de la classe de modèle pour la table markers
class Marker(Model):
    id = IntegerField(primary_key=True)
    marker_id = CharField(unique=True)
    marker_type = CharField()
    lat = FloatField()
    lng = FloatField()
    mp_type = CharField(null=True)
    icon = CharField(null=True)
    name = CharField(null=True)
    quality = CharField(null=True)
    title = CharField(null=True)
    season = CharField(null=True)
    weather = CharField(null=True)

    class Meta:
        database = db
        table_name = 'markers'
        # constraints = [
        #     Check('marker_id LIKE "npc_%" OR marker_id LIKE "boss_%"'),
        #     Check('marker_type IN ("boss", "mp", "npc", "tribu")'),
        #     Check('quality IN ("suprème", "excellent", "choix", "fin", "base") OR quality IS NULL')
        # ]


class Users(UserMixin, Model):
    id = IntegerField(primary_key=True)
    name = CharField(unique=True)
    password = CharField(null=True)

    class Meta:
        database = db
        table_name = 'users'


class AllowedGuilds(Model):
    id = IntegerField(primary_key=True)
    guild_id = CharField(unique=True)
    guild_name = CharField(unique=True)

    class Meta:
        database = db
        table_name = 'AllowedGuilds'


class AllowedChar(Model):
    id = IntegerField(primary_key=True)
    char_name = CharField(unique=True)

    class Meta:
        database = db
        table_name = 'AllowedChar'
# Création de la table markers si elle n'existe pas
with db:
    db.create_tables([Users, Marker, AllowedGuilds, AllowedChar])
